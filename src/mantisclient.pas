unit mantisclient;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, db, sqldb, pqconnection, fpjson;

type
  TLogEvent = Procedure (Sender : TObject; Const aMessage : string) of object;

  { TMantisClient }

  TMantisClient = Class (TObject)
  private
    FConn: TSQLConnection;
    FOnLOg: TLogEvent;
  Protected
    procedure DoLog(const aMessage : string); overload;
    procedure DoLog(const aFmt : string; aArgs : Array of const);  overload;
    function CreateQuery(const aSQL: String; aParams : Array of const): TSQLQuery;
    function GetTrans: TSQLTransaction;
    procedure DatasetToObject(aDataset: TDataset; aObject: TJSONObject; aFields: array of string);
    procedure DatasetToArray(aDataset: TDataset; aArray: TJSONArray; aFields: array of String);
  Public
    Constructor Create; virtual;
    destructor destroy; override;
    Procedure Connect;
    Procedure DisConnect;
    Function GetSingleRecord(aSQL : String; aParams : Array of const; aFields: array of string) : TJSONObject;
    Function GetRecordList(aSQL : String; aParams : Array of const; aFields: array of string) : TJSONArray;
    Function GetTableRecords(aTableName : String; aFields: array of string; aStartID : Integer = -1; aCount : Integer = 0) : TJSONArray;
    property conn : TSQLConnection read FConn;
    property trans : TSQLTransaction Read GetTrans;
    Property OnLog : TLogEvent Read FOnLOg Write FONLog;
  end;



implementation

uses dateutils;

{ TMantisClient }

function TMantisClient.GetTrans: TSQLTransaction;
begin
  Result:=FConn.Transaction;
end;

procedure TMantisClient.DatasetToObject(aDataset: TDataset;
  aObject: TJSONObject; aFields: array of string);

  Function FieldToJSON(aField : TField) : TJSONData;

  begin
    Case aField.DataType of
      ftBoolean : Result:=TJSONBoolean.Create(aField.AsBoolean);
      ftInteger,
      ftSmallint,
      ftWord,
      ftAutoInc : Result:=TJSONIntegerNumber.Create(aField.AsInteger);
      ftLargeInt : Result:=TJSONInt64Number.Create(aField.AsLargeInt);
      ftFixedChar,
      ftString : Result:=TJSONString.Create(aField.AsString);
      ftWideString,
      ftWideMemo,
      ftFixedWideChar : Result:=TJSONString.Create(aField.AsUTF8String);
      ftDate,
      ftDateTime,
      ftTime,
      ftTimeStamp : Result:=TJSONString.Create(DateToISO8601(aField.AsDateTime));
      ftMemo : Result:=TJSONString.Create(aField.AsString);
    else
      Raise Exception.Create('Field '+aField.FieldName+' has unsupported fieldtype');
    end;
  end;

Var
  I : Integer;
  FN : String;

begin
  if Length(aFields)=0 then
    begin
    For I:=0 to aDataset.Fields.Count-1 do
      aObject.Add(aDataset.Fields[i].FieldName,FieldToJSON(aDataset.Fields[i]));
    end
  else
    begin
    For FN in aFields do
      aObject.Add(FN,FieldToJSON(aDataset.FieldByName(FN)));
    end;
end;

procedure TMantisClient.DatasetToArray(aDataset: TDataset; aArray: TJSONArray;
  aFields: array of String);
var
  Obj:TJSONObject;
begin
  While not aDataset.EOF do
    begin
    Obj:=TJSONObject.Create;
    aArray.Add(Obj);
    DatasetToObject(aDataset,Obj,aFields);
    aDataset.Next;
    end;
end;

constructor TMantisClient.Create;
begin
  FConn:=TPQConnection.Create(Nil);
  FConn.Transaction:=TSQLTransaction.Create(FConn);
end;

destructor TMantisClient.destroy;
begin
  Disconnect;
  FreeAndNil(FConn);
  inherited destroy;
end;

procedure TMantisClient.Connect;
begin
  FConn.Connected:=False;
  FConn.Connected:=True;
end;

procedure TMantisClient.DisConnect;
begin
  if FConn.Transaction.Active then
    FConn.Transaction.Rollback;
  FConn.Connected:=False;
end;

procedure TMantisClient.DoLog(const aMessage: string);
begin
  If Assigned(FOnLog) then
    FOnLog(Self,aMessage);
end;

procedure TMantisClient.DoLog(const aFmt: string; aArgs: array of const);
begin
  DoLog(Format(aFmt,aArgs));
end;

function TMantisClient.CreateQuery(const aSQL: String;
  aParams: array of const): TSQLQuery;

Var
  iCnt: Integer;
  sName: string;
  pParam: TParam;

begin
  if Length(aParams) mod 2 <> 0 then
    raise EArgumentException.Create('Odd number of parameters not allowed');
  Result:=TSQLQuery.Create(FConn);
  Result.Database:=FConn;
  Result.Transaction:=FConn.Transaction;
  Result.SQL.Text:=aSQL;
  try
    iCnt := 0;
    while iCnt < Length(aParams)-1 do
      begin
      with TVarRec(aParams[iCnt]) do
        case VType of
          vtChar:
            sName := string(VChar);
          vtString:
            sName := string(VString^);
          vtPChar:
            sName := string(VPChar);
          vtWideChar:
            sName := VWideChar;
          vtPWideChar:
            sName := VPWideChar;
          vtAnsiString:
            sName := string(ansistring(VAnsiString));
          vtWideString:
            sName := widestring(VWideString);
          vtUnicodeString:
            sName := unicodestring(VUnicodeString);
        else
          raise EArgumentException.CreateFmt('Parameters need to be name/value pairs. No name found at position %d', [iCnt]);
        end;
      pParam := Result.ParamByName(sName);
      with TVarRec(aParams[iCnt+1]) do
        case VType of
           vtInteger:
             pParam.AsInteger := VInteger;
           vtBoolean:
             pParam.AsBoolean := VBoolean;
           vtChar:
             pParam.AsString := string(VChar);
           vtExtended:
             pParam.asFloat := VExtended^;
           vtString:
             pParam.AsString := string(VString^);
           vtPChar:
             pParam.AsString := string(VPChar);
           vtWideChar:
             pParam.AsString := VWideChar;
           vtPWideChar:
             pParam.AsString := VPWideChar;
           vtAnsiString:
             pParam.AsString := string(ansistring(VAnsiString));
           vtCurrency:
             pParam.AsCurrency := VCurrency^;
           vtVariant:
             pParam.Value := VVariant^;
           vtWideString:
             pParam.AsString := widestring(VWideString);
           vtInt64:
             pParam.AsLargeInt := VInt64^;
           vtUnicodeString:
             pParam.AsString := unicodestring(VUnicodeString);
           vtPointer, vtClass, vtObject:
             pParam.Clear; // can only be Nil
         else
           raise EArgumentException.CreateFmt('Unsuported value type at position %d', [iCnt+1]);
         end;
     Inc(iCnt, 2);
     end;
  except
    Result.Free;
    Raise;
  end;
end;

function TMantisClient.GetSingleRecord(aSQL: String; aParams: array of const;
  aFields: array of string): TJSONObject;
Var
  Qry : TSQLQuery;

begin
  Qry:=CreateQuery(aSQL,aParams);
  try
    Qry.Open;
    if not Qry.IsEmpty then
      begin
      Result:=TJSONObject.Create;
      DatasetToObject(Qry,Result,aFields);
      end
    else
      Result:=Nil;
  finally
    Qry.Free;
  end;
end;

function TMantisClient.GetRecordList(aSQL: String; aParams: array of const;
  aFields: array of string): TJSONArray;

Var
  Qry : TSQLQuery;

begin
  Qry:=CreateQuery(aSQL,aParams);
  try
    Qry.Open;
    Result:=TJSONArray.Create;
    DatasetToArray(Qry,Result,aFields);
  finally
    Qry.Free;
  end;
end;

function TMantisClient.GetTableRecords(aTableName: String;
  aFields: array of string; aStartID: Integer; aCount: Integer): TJSONArray;

Var
  SQL : String;

begin
  SQL:='select * from '+aTableName;
  if aStartID<>-1 then
    SQL:=SQL+Format(' where (id>=%d)',[aStartID]);
  Sql:=SQL+' order by '+aTableName+'.id';
  if aCount>0 then
    SQL:=SQL+' LIMIT '+IntToStr(aCount);
  Result:=GetRecordList(SQL,[],aFields);
end;

end.

