unit mgconvert;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Math, gitlabclient, mantisclient, fpjson, dateutils,
  contnrs;

Const
  StatusResolved = 80;
  StatusClosed = 90;

  SleepInterval = 128; // Avoid getting hit by the API throttle
  LongSleepInterval = 60 * 1000; // if we're hit anyway, wait 1 minute.

Var
  CategoryColor : string = 'CadetBlue';
  VersionColor: String = 'orange';
  FixVersionColor : string  = 'green';
  TargetVersionColor : string  = 'darkolivegreen';
  ExtraFieldColor : string  = 'darkslateblue';

  SeverityColor : string  = 'coral';
  ReproducabilityColor : string  = 'cornsilk';
  StatusColor : string  = 'darkgreen';
  PriorityColor : string  = 'maroon';
  ResolutionColor : string  = 'purple';

Const
  SlabelPrefixReproducibility = 'Reproducibility';
  SlabelPrefixSeverity = 'Severity';
  SlabelPrefixStatus = 'Status';
  SlabelPrefixPriority = 'Priority';
  SlabelPrefixResolution = 'Resolution';
  SLabelPrefixCategory = 'Category';
  SLabelPrefixVersion = 'Version';
  sLabelPrefixFixVersion = 'FixVersion';
  sLabelPrefixTargetVersion = 'TargetVersion';

  BUG_REL_NONE   = -2;
  BUG_REL_ANY    = -1;
  BUG_DUPLICATE  = 0;
  BUG_RELATED    = 1;
  BUG_DEPENDANT  = 2;
  BUG_BLOCKS     = 3;
  BUG_HAS_DUPLICATE = 4;

  SRelatesTo = 'relates_to';
  SBlockedBy = 'is_blocked_by';
  SBlocks = 'blocks';


Type
  TDescriptionFlag = (dfAssignee,dfAssigneeEmail,dfSubmitterEmail);
  TDescriptionFlags = Set of TDescriptionFlag;

  TPart = (pStandardLabels,pVersionLabels,pSkipLabelEqMilestone,pCategoryLabels,
    pVersionMileStones,pCleanMileStones,pPriorityAsWeight,pSkipReproducibility,pSkipDefaultLabels,
    pBugs,pComments,pFiles,pPrivateComments,pExtraFieldLabels,pLinkIssues, pLazLabelColors);
  TParts = Set of TPart;

  EConverter = class(Exception);

  { TConversionConfig }

  TConversionConfig = Record
    GitlabProjectID : Int64;
    MantisProjectID : Int64;
    StartID : Int64;
    Count : Integer;
    FixedInRevisionField : String;
    TargetVersionLabelField : String;
    ExtraFieldLabel : String;
    ExtraFieldName : string;
    ExtraFieldUnscoped : Boolean;
    BugFileDir : String;
    Parts : TParts;
    UserMapFile : String;
    ProjectMapFile : String;
    UserMapCategory: String;
    UserMapProjectID: Int64;
    GitlabUserIDMapFile:String;
    RevisionMapFile : string;
    ExcludeCategories : String;
    OnlyCategories : String;
  end;

  { TMantisGitlabConverter }

  TMantisGitlabConverter = Class
  private
    FConfig: TConversionConfig;
    FGitlab : TGitlabClient;
    FMantis : TMantisClient;
    FOnLog: TLogEvent;
    FMilestoneMap : TStringList;
    FReproducabilityMap : TStringList;
    FStatusMap : TStringList;
    FResolutionMap : TStringList;
    FPriorityMap : TStringList;
    FSeverityMap : TStringList;
    // Mantis user name -> Gitlab user name
    FUserMap : TFPStringHashTable;
    FProjectMap : TFPStringHashTable;
    FVersions : TStringList;
    FProdVersions : TStringList;
    // Gitlab user name -> gitlab user ID
    FGitlabUserIDMap : TStringList;
    FNotificationlevel : String;
    FRevisionToHash: TFPStringHashTable;
  Protected
    // auxiliary functions
    class function EmptyValue(aValue: String): Boolean;
    class function ToSQLList(const aList: String): string;
    procedure DoLog(const aMessage : string); overload;
    procedure DoLog(const aFmt : string; aArgs : Array of const);  overload;
    function DoPart(aPart : TPart) : Boolean;
    function FormatUserText(aText: string): string;
    function CleanMileStoneName(aMileStone: String): String;
    function CreateLabel(const aValue, aPrefix, aDescrPrefix, aColor: string; Unscoped : boolean = false): Boolean;
    function ConvertRevToHash(AText: String): String;
    // Mappings
    function MapUser(const aName, aRealName, aEmail: string): String;
    function MapProjectID(aProjectID: Integer): Integer;
    function MapMileStone(aVersion: string): Int64;
    function MapGitlabUserID(aUser: string): PtrInt;
    function GetGitlabUserNameFromID(aUserID: Int64): String;
    // Get extra info
    function GetMonitors(aBugID: Int64): String;
    function GetBugLabels(aSrc: TJSONObject; aMileStone: int64 = 0): String;virtual;
    function GetDescription(aSrc: TJSONObject; Flags: TDescriptionFlags; const aReporterGitlabUser, aAssignedGitlabUser: String): String; virtual;
    procedure SetNotificationLevel(const aLevel: String);
    procedure SaveNotificationLevel; virtual;
    procedure RestoreNotificationLevel; virtual;
    procedure WriteGitlabUserMap; virtual;
    procedure CollectVersionStrings(aVersions, aProdVersions: TStrings);
    procedure CreateLabelsFromMap(aMap: TStringList; aPrefix: String; aColor: String);
    procedure LoadUserMap; virtual;
    Procedure LoadGitlabUserMap; virtual;
    procedure LoadMantisUserMap; virtual;
    procedure LoadProjectMap; virtual;
    Procedure LoadRevisionMap; virtual;
    Procedure CreateStandardMaps; virtual;
    procedure ConvertStandardLabels; virtual;
    procedure ConvertVersionLabels; virtual;
    procedure ConvertBugs; virtual;
    procedure ConvertCategoryLabels; virtual;
    procedure ConvertExtraFieldLabel; virtual;
    procedure ConvertVersionMilestones; virtual;
    procedure CreateMileStoneMap;virtual;
    procedure ConvertBugNotes(aOldBugID, aNewBugID: Int64); virtual;
    procedure ConvertBugFiles(aOldBugID, aNewBugID: Int64); virtual;
    procedure ConvertBugLinks; virtual;
    procedure CopyBugData(aSrc, aDest: TJSONObject);virtual;
    property Versions : TStringList Read FVersions;
    property ProdVersions: TStringList Read FProdVersions;
  Public
    Constructor create(aConfig : TConversionConfig; aGitlab : TGitlabClient; aMantis : TMantisClient);
    Destructor destroy; override;
    procedure CreateLink(SrcID, TargetID, TargetProjectID, DupID: Integer; const LinkType: string);
    Procedure Execute;
    procedure CheckConfig; virtual;
    Function GetProjectResourceURL(aResource : string) : String;
    Property Config : TConversionConfig Read FConfig;
    Property OnLog : TLogEvent Read FOnLog Write FOnLog;
  end;

implementation

uses strutils;

{From LazStringUtils}

function GetPart(const ASkipTo, AnEnd: array of String; const ASource: String): String;

var
  n, i, idx: Integer;
  Source, Match: String;
  HasEscape: Boolean;
begin
  Source := ASource;

  if High(ASkipTo) >= 0
  then begin
    idx := 0;
    Match := '';
    HasEscape := False;
    for n := Low(ASkipTo) to High(ASkipTo) do
    begin
      if ASkipTo[n] = ''
      then begin
        HasEscape := True;
        Continue;
      end;
      i := Pos(ASkipTo[n], Source);
      if i > idx
      then begin
        idx := i;
        Match := ASkipTo[n];
      end;
    end;
    if (idx = 0) and not HasEscape then Exit('');
    if idx > 0
    then Delete(Source, 1, idx + Length(Match) - 1);
  end;

  idx := MaxInt;
  for n := Low(AnEnd) to High(AnEnd) do
  begin
    if AnEnd[n] = '' then Continue;
    i := Pos(AnEnd[n], Source);
    if (i > 0) and (i < idx) then idx := i;
  end;

  if idx = MaxInt
  then begin
    Result := Source;
    Source := '';
  end
  else begin
    Result := Copy(Source, 1, idx - 1);
    Delete(Source, 1, idx - 1);
  end;
end;

function Dash2Empty(s: string): string;
begin
  Result := s;
  if Result = '-' then
    Result := '';
end;

{ TMantisGitlabConverter }

constructor TMantisGitlabConverter.create(aConfig: TConversionConfig; aGitlab : TGitlabClient; aMantis : TMantisClient);
begin
  FConfig:=aConfig;
  FGitLab:=aGitlab;
  FMantis:=aMantis;
  FMilestoneMap:=TStringList.Create;
  FReproducabilityMap:=TStringList.Create;
  FStatusMap:=TStringList.Create;
  FResolutionMap:=TStringList.Create;
  FPriorityMap:=TStringList.Create;
  FSeverityMap:=TStringList.Create;
  FUserMap:=TFPStringHashTable.Create;
  FProjectMap:=TFPStringHashTable.Create;
  FVersions:=TStringList.Create;
  FVersions.Sorted:=True;
  FVersions.Duplicates:=dupIgnore;
  FProdVersions:=TStringList.Create;
  FProdVersions.Sorted:=True;
  FProdVersions.Duplicates:=dupIgnore;
  FGitlabUserIDMap:=TStringList.Create;
  FGitlabUserIDMap.Sorted:=True;
  FGitlabUserIDMap.Duplicates:=dupIgnore;
  FRevisionToHash := TFPStringHashTable.Create;

  if DoPart(pLazLabelColors) then begin
    VersionColor := '#FFECE0';
  end;

  CreateStandardMaps;
  // ReadUserMap;
end;

destructor TMantisGitlabConverter.destroy;
begin
  FreeAndNil(FRevisionToHash);
  FreeAndNil(FMilestoneMap);
  FreeAndNil(FReproducabilityMap);
  FreeAndNil(FStatusMap);
  FreeAndNil(FResolutionMap);
  FreeAndNil(FPriorityMap);
  FreeAndNil(FSeverityMap);
  FreeAndNil(FVersions);
  FreeAndNil(FProdVersions);
  FreeAndNil(FUserMap);
  FreeAndNil(FProjectMap);
  FreeAndNil(FGitlabUserIDMap);
  inherited destroy;
end;

function TMantisGitlabConverter.GetProjectResourceURL(aResource: string): String;

begin
  Result:=Format('projects/%d/%s/',[FConfig.GitlabProjectID,aResource]);
end;

function TMantisGitlabConverter.DoPart(aPart: TPart): Boolean;
begin
  Result:=aPart in FConfig.Parts;
end;

procedure TMantisGitlabConverter.DoLog(const aMessage: string);
begin
  If Assigned(OnLog) then
    OnLog(Self,aMessage);
end;

procedure TMantisGitlabConverter.DoLog(const aFmt: string;
  aArgs: array of const);
begin
  DoLog(Format(aFmt,aArgs));
end;

procedure TMantisGitlabConverter.CreateStandardMaps;

Var
  FMap : TstringList;
  Prefix : String;

  Procedure SetMap(aMap : TStringList; aPrefix : String);

  begin
    FMap:=aMap;
    Prefix:=aPrefix;
  end;

  Procedure Define(aName : String; aValue : Integer);
  begin
    If (Prefix<>'') and (Pos(Prefix,aName)=1) then
      Delete(aName,1,Length(Prefix));
    aName:=UpperCase(Copy(aName,1,1))+LowerCase(Copy(aName,2,Length(aName)-1));
    aName:=StringReplace(aName,'_',' ',[rfReplaceAll]);
    FMap.Add(IntToStr(aValue)+'='+aName);
  end;


begin
  SetMap(FReproducabilityMap,'REPRODUCIBILITY_');
  define( 'REPRODUCIBILITY_ALWAYS', 10 );
  define( 'REPRODUCIBILITY_SOMETIMES', 30 );
  define( 'REPRODUCIBILITY_RANDOM', 50 );
  define( 'REPRODUCIBILITY_HAVE_NOT_TRIED', 70 );
  define( 'REPRODUCIBILITY_UNABLE_TO_DUPLICATE', 90 );
  define( 'REPRODUCIBILITY_NOT_APPLICABLE', 100 );
  SetMap(FStatusMap,'');
  define( 'FEEDBACK', 20 );
  if not DoPart(pSkipDefaultLabels) then
    define( 'ACKNOWLEDGED', 30 );
  define( 'CONFIRMED', 40 );
  if not DoPart(pSkipDefaultLabels) then begin
    define( 'ASSIGNED', 50 );
    define( 'RESOLVED', 80 );
    define( 'CLOSED', 90 );
  end;
  SetMap(FResolutionMap,'');
  if not DoPart(pSkipDefaultLabels) then begin
    define( 'OPEN', 10 );
    define( 'FIXED', 20 );
    define( 'REOPENED', 30 );
  end;
  define( 'UNABLE_TO_REPRODUCE', 40 );
  define( 'NOT_FIXABLE', 50 );
  define( 'DUPLICATE', 60 );
  define( 'NOT_A_BUG', 70 );
  define( 'SUSPENDED', 80 );
  define( 'WONT_FIX', 90 );
  SetMap(FPriorityMap,'');
  define( 'NONE', 10 );
  define( 'LOW', 20 );
  define( 'NORMAL', 30 );
  define( 'HIGH', 40 );
  define( 'URGENT', 50 );
  define( 'IMMEDIATE', 60 );
  SetMap(FSeverityMap,'');
  define( 'FEATURE', 10 );
  define( 'TRIVIAL', 20 );
  define( 'TEXT', 30 );
  define( 'TWEAK', 40 );
  if not DoPart(pSkipDefaultLabels) then
    define( 'MINOR', 50 );
  define( 'MAJOR', 60 );
  define( 'CRASH', 70 );
  define( 'BLOCK', 80 );


end;

procedure TMantisGitlabConverter.CreateLabelsFromMap(aMap : TStringList; aPrefix : String; aColor : String);

Var
  I,aCount : Integer;
  ID : Int64;
  Value : string;
  Dest : TJSONObject;

begin
  aCount:=0;
  DoLog('Converting %d "%s" labels',[aMap.Count,aPrefix]);
  For I:=0 to aMap.Count-1 do
      begin
      Value:=aMap.ValueFromIndex[I];
      if Value<>'' then
        begin
        Dest:=TJSONObject.Create(['name',aPrefix+'::'+Value,'description','Issue '+aPrefix+': '+Value,'color',aColor]);
        try
          ID:=FGitLab.CreateResource(GetProjectResourceURL('labels'),Dest);
          DoLog(aPrefix+' label "%s" created with ID %d',[Value,ID]);
          inc(aCount);
        finally
          Dest.Free;
        end;
        end;
      end;
  DoLog('Successfully converted %d labels for %s with color %s.',[aCount,aPrefix,aColor]);
end;

procedure TMantisGitlabConverter.ConvertStandardLabels;

begin
  if not DoPart(pSkipReproducibility) then
    CreateLabelsFromMap(FReproducabilityMap,SlabelPrefixReproducibility,ReproducabilityColor);
  CreateLabelsFromMap(FSeverityMap,SlabelPrefixSeverity,SeverityColor);
  CreateLabelsFromMap(FStatusMap,SlabelPrefixStatus,StatusColor);
  if not DoPart(pPriorityAsWeight) then
    CreateLabelsFromMap(FPriorityMap,SlabelPrefixPriority,PriorityColor);
  CreateLabelsFromMap(FResolutionMap,SlabelPrefixResolution,ResolutionColor);
end;

procedure TMantisGitlabConverter.ConvertCategoryLabels;

Const
  SQLSelect = 'select name,status from mantis_category_table '+
              ' where (project_id=:ID)  and not (name in ('''',''-''))';

Var
  SrcLabels : TJSONArray;
  aObj : TJSONObject;
  Value : TJSONStringType;
  aCount,I : Integer;

begin
  SrcLabels:=FMantis.GetRecordList(SQLSelect,['ID',FConfig.MantisProjectID],[]);
  try
    aCount:=0;
    DoLog('Converting %d category labels',[SrcLabels.Count]);
    For I:=0 to SrcLabels.Count-1 do
      if SrcLabels[i].JSONType=jtObject then
        begin
        aObj:=SrcLabels.Objects[i];
        Value:=aObj.Get('name','');
        if not EmptyValue(Value) then
          begin
          if CreateLabel(Value,SLabelPrefixCategory,'Issue category: %s',CategoryColor) then
            Inc(aCount);
          end;
        end;
    DoLog('Successfully converted %d category labels.',[aCount]);
  finally
    SrcLabels.Free;
  end;
end;

procedure TMantisGitlabConverter.ConvertExtraFieldLabel;

Const
  SQLSelect = 'select f.possible_values from '+
              ' mantis_custom_field_project_table fp '+
              ' inner join mantis_custom_field_table f on (fp.field_id=f.id) '+
              ' where  (fp.project_id=:ID)  and (f.name=:Name)';

Var
  Src : TJSONObject;
  Values : String;
  Value : String;
  aCount : Int64;

begin
  aCount:=0;
  src:=nil;
  if (FConfig.ExtraFieldName='') or (FConfig.ExtraFieldLabel='') then
    DoLog('ExtraFieldName and ExtraFieldLabel must be set to convert extra field label')
  else
    begin
    Src:=FMantis.GetSingleRecord(SQLSelect,['ID',FConfig.MantisProjectID, 'Name',FConfig.ExtraFieldName],[]);
    try
      Values:=Src.Get('possible_values','');
      DoLog('%s values: %s',[FConfig.ExtraFieldName,Values]);
      aCount:=0;
      for value in SplitString(Values,'|') do
        if not EmptyValue(Value) then
          begin
          if CreateLabel(value,FConfig.ExtraFieldLabel,FConfig.ExtrafieldLabel+': %s',ExtraFieldColor,FConfig.ExtrafieldUnscoped) then
            Inc(aCount);
          end;
    finally
      Src.Free;
    end;
    end;
end;

function CharAt(const t: String; i: Integer): Char; inline;
begin
  if (i > Length(t)) or (i < 1) then
    Result := #0
  else
    Result := t[i];
end;

type
  CharSet = set of char;
function CharIn(const t: String; i: Integer; exp: CharSet): boolean; inline;
begin
  if (i > Length(t)) or (i < 1) then
    Result := False
  else
    Result := t[i] in exp;
end;

function TMantisGitlabConverter.FormatUserText(aText: string): string;
var
  lines: TStringList;
  CurLine, CurBlockStart, CntCodeLines: Integer;
  LastCodeBlockEnd, CntPreFmtLines, k, k2: integer;
  CurLineTxt, CurLineLowerTxt: String;
  isPas, isPascal, wasPascal, isPatch, IsXml, ignoreEmptyLine: Boolean;

  function EscapeHtml(aTxt: String; aEscPre: Boolean = False; aEscapeInPre: Boolean = False): String;
  var
    LtPos: SizeInt;
  begin
    Result := aTxt;
    LtPos := pos('<', Result);
    while LtPos > 0 do begin
      if ( CharIn(Result, LtPos+1, ['a'..'z', 'A'..'Z', '/', '!', '?', '#']) or
           (aEscapeInPre and not (CharAt(Result, LtPos+1) in [' ', #9]) )
         ) and
         ( aEscPre or
           not ( ( (LowerCase(copy(Result, LtPos+1, 3)) = 'pre') and
                   CharIn(Result, LtPos+4, ['>', ' ', #9]) ) or
                 ( (LowerCase(copy(Result, LtPos+1, 4)) = '/pre') and
                   CharIn(Result, LtPos+5, ['>', ' ', #9]) )
               )
         )
      then begin
        Result[LtPos] := '&';
        Insert('LtPos;', Result, LtPos+1);
      end;

      LtPos := PosEx('<', Result, LtPos + 1);
    end;
  end;

  procedure CheckBlock;
  var
    a, b, CntBlockLines: Integer;
    LnTxt, CodeTypeStr: String;
    MaybeContinuation: Boolean;
  begin
    try
      MaybeContinuation := (CurBlockStart = LastCodeBlockEnd + 2) and (LastCodeBlockEnd > 0);
      CntBlockLines := CurLine-CurBlockStart;

      if ( (CntCodeLines >= 2)  and (CntCodeLines >= CntBlockLines * 2 div 3) ) or
         ( (CntBlockLines <= 2) and (CntCodeLines >= 1) and MaybeContinuation )  // 1 or 2 line block
      then
        begin
        // Detected Code
        if MaybeContinuation then // continue last block
          begin
          lines.Delete(LastCodeBlockEnd);
          lines.Insert(CurLine-1, '```');
          LastCodeBlockEnd := CurLine-1;
          end
        else
          begin
          LnTxt := trim(lines[CurBlockStart]);
          if (CharAt(LnTxt, Length(LnTxt)) = ':') then               // Header line ?
            inc(CurBlockStart);

          CodeTypeStr := '';
          if isPatch then
            CodeTypeStr := ' diff'
          else
          if isPascal then
            CodeTypeStr := ' pascal'
          else
          if IsXml then
            CodeTypeStr := ' xml';

          lines.Insert(CurLine, '```');
          lines.Insert(CurBlockStart, '```' + CodeTypeStr);
          LastCodeBlockEnd := CurLine+1;
          inc(CurLine, 2);
          end;
        end
      else
      if (CntPreFmtLines >= 2) and (CntPreFmtLines >= CntBlockLines * 2 div 3) then
        begin
        // Detected <pre> data
        if CurLine >= lines.Count then
          CurLine := lines.Count - 1;
        for a := CurBlockStart to CurLine do
          lines[a] := EscapeHtml(lines[a], False, True);

        LnTxt := trim(lines[CurBlockStart]);
        if (CharAt(LnTxt, Length(LnTxt)) = ':') and
            CharIn(LnTxt, 1, ['A'..'Z', 'a'..'z'])
        then
          inc(CurBlockStart);

        lines.Insert(CurLine, '</pre>');
        lines.Insert(CurBlockStart, '<pre>');
        LastCodeBlockEnd := -1;
        inc(CurLine, 2);
        end
      else
        begin
        // Normal text
        LastCodeBlockEnd := -1;
        if CurLine >= lines.Count then
          CurLine := lines.Count - 1;
        a := CurBlockStart - 1;
        while a < CurLine do
          begin
          inc(a);
          LnTxt := lines[a];

          // indent of >= 2 then treat as single line code
          if CharIn(LnTxt, 1, [' ',#9]) and
             CharIn(LnTxt, 2, [' ',#9])
          then
            begin
            b := a + 1;
            while (b <= CurLine) and
              CharIn(lines[b], 1, [' ',#9]) and
              CharIn(lines[b], 2, [' ',#9])
            do
              inc(b);
            dec(b);
            if b > a then
              begin
              if (a >= CurBlockStart + 2) and
                (not CharIn(lines[a-1], Length(lines[a-1])-Length('<br/>'), [':'])) and
                (    CharIn(lines[a-2], Length(lines[a-2])-Length('<br/>'), [':']))
              then
                dec(a);
              lines.Insert(b+1, '```');
              lines.Insert(a, '```');
              a := b + 2;
              inc(CurLine, 2);
              end
            else
              begin
              lines[a] := '```' +LnTxt+'```<br/>';
              end;
            end
          else
            begin
            // need escaping
            if CharIn(LnTxt, 1,  ['#', '=', '-', '\', '+', '*']) and
               not( CharIn(LnTxt, 1, ['#']) and CharIn(LnTxt, 2, ['0'..'9']) )
            then
              LnTxt := '\' +LnTxt;

            LnTxt := EscapeHtml(LnTxt);

            if CharIn(LnTxt, 1, [' ', #9]) and
               CharIn(trim(LnTxt),1, ['#', '=', '-', '*', '+'])
            then
              LnTxt := '&nbsp;' +LnTxt;

            if (LnTxt <> '') and
               //( (LnTxt[length(LnTxt)] in [':']) or
               //  (Length(LnTxt) < 40) // short line
               //) and
               (a < lines.Count-1) and (lines[a+1] <> '')
            then
              LnTxt := LnTxt+'<br/>';

            lines[a] := LnTxt;
            end;
          end;
        end;
    except
      isPascal := False;
      isPatch := False;
      IsXml := False;
      LastCodeBlockEnd := -1;
      DoLog('Error in usertext formating');
    end;
  end;

  procedure ResetBlock;
  begin
    CurBlockStart := CurLine+1;
    CntCodeLines := 0;
    CntPreFmtLines := 0;
    ignoreEmptyLine := False;

    wasPascal := isPascal;
    isPascal := False;
    isPatch := False;
    IsXml := False;
  end;

begin
  Result := aText;
  lines := TStringList.Create;
  try try
    lines.Text := aText;
    CurLine := -1;
    CurBlockStart := 0;
    CntCodeLines := 0;
    CntPreFmtLines := 0;
    LastCodeBlockEnd := -1;
    isPascal := False;
    isPatch := False;
    IsXml := False;
    ignoreEmptyLine := False;
    while CurLine < lines.Count - 1 do begin
      inc(CurLine);
      CurLineTxt := lines[CurLine];

      if CurLineTxt = '' then begin
        if ignoreEmptyLine then
          Continue;
        CheckBlock;
        ResetBlock;
        continue;
      end;
      ignoreEmptyLine := False;

      if (CurLineTxt = '```') or (copy(CurLineTxt, 1, 4) = '``` ') then begin
        k := CurLine + 1;
        while (k < lines.Count) and (lines[k] <> '```') do
          inc(k);
        if k < lines.Count then begin
          k := k - CurLine;
          dec(CurLine);
          CheckBlock;
          CurLine := CurLine + k + 1;
          ResetBlock;
          continue;
        end;

        lines[CurLine] := '` ` ` ';  // unclosed, make opening unrecognizable
      end;

      CurLineLowerTxt := LowerCase(CurLineTxt);
      if (pos('<pre>', CurLineLowerTxt) > 0) or (pos('<pre ', CurLineLowerTxt) > 0) then begin
        k := CurLine + 1;
        while (k < lines.Count) and
          (pos('</pre>', lines[k]) <= 0) and (pos('</pre ', lines[k]) <= 0)
        do
          inc(k);
        if k < lines.Count then begin
          k := k - CurLine;
          dec(CurLine);
          CheckBlock;
          for k2 := CurLine+1 to CurLine + k + 1 do
            lines[k2] := EscapeHtml(lines[k2], False, True);
          CurLine := CurLine + k + 1;
          ResetBlock;
          continue;
        end;

        lines[CurLine] := EscapeHtml(lines[CurLine], True);  // unclosed, make opening unrecognizable
      end;

      try
        isPas :=
           ( IndexText(GetPart([''], [' ', #9, ';', '('], CurLineTxt),
             ['function', 'procedure', 'begin', 'class', 'var', 'type', 'const',
              'program', 'unit', 'uses'
             ]
             ) >= 0
           ) or
           ( wasPascal and (IndexText(Trim(CurLineLowerTxt), ['implementation', 'interface']) >= 0) )
           ;

        ignoreEmptyLine :=
           (  IndexText(GetPart([''], [' ', #9, ';', '('], trim(CurLineTxt)),
             ['program', 'uses', '{$mode']
             ) >= 0
           ) or
           ( (IndexText(GetPart([''], [' ', #9], trim(CurLineTxt)),
              ['unit']
              ) >= 0) and
             (pos(';', CurLineTxt) > 0)
           ) or
           ( wasPascal and (IndexText(Trim(CurLineLowerTxt), ['implementation', 'interface']) >= 0) )
           ;

        isPascal := isPascal or isPas or
          ( (CurLineTxt[Length(CurLineTxt)] = ';') and
            (pos(':=', CurLineTxt) > 0)
          );

        k := pos('</', CurLineTxt);
        IsXml := IsXml or
          ( (k > 0) and
            CharIn(CurLineTxt, k+2, ['A'..'Z', 'a'..'z']) and
            (pos('>', CurLineTxt) > 0)
          );

        isPatch := isPatch or
          ( ((copy(CurLineTxt,1,4) = '--- ') or (copy(CurLineTxt,1,4) = '+++ ')) and
            (CurLine < lines.Count-1) and
            (copy(lines[CurLine+1],1,3) = '@@ ')
          );

        if (CurLineTxt[1] in [' ', #9]) or
           ( isPatch and (CurLineTxt[1] in ['+', '-'])) or
           ( CharIn(CurLineTxt, 1, ['#', '$']) and CharIn(CurLineTxt, 2, ['0'..'9']) ) or
           ( CharIn(CurLineTxt, 1, ['0'])      and CharIn(CurLineTxt, 2, ['x']) and CharIn(CurLineTxt,3,['0'..'9', 'a'..'f', 'A'..'F']) ) or
           ( copy(CurLineTxt,1,5) = '(gdb)' ) or
           ( copy(CurLineTxt,1,3) = 'end' ) or
           ( (isPascal or wasPascal) and (copy(CurLineTxt,1,2) = '{$') ) or
           ( isPas )
        then
          inc(CntCodeLines); // code line

        if (CurLineTxt[1] in [' ', #9]) or
           ( CharIn(CurLineTxt, 1, ['#','~','=']) and CharIn(CurLineTxt, 2, [' ', '#', '-', '=', '~', '>', '"', #9]) )
        then
          inc(CntPreFmtLines) // maybe some kind of list
        else
        if ( CharIn(CurLineTxt,1, ['-','*']) and CharIn(CurLineTxt, 2, ['#', '-', '=', '~', '>', '"']) )
        then
          inc(CntPreFmtLines); // maybe some kind of list
      except
        DoLog('Error in usertext formatting (line)');
      end;
      wasPascal := isPascal;

    end;
    inc(CurLine);
    CheckBlock;
    Result := lines.Text;
  except
    DoLog('Error in usertext formatting (all text)');
  end;
  finally
    lines.Free;
  end;
end;

function TMantisGitlabConverter.CleanMileStoneName(aMileStone: String): String;
var
  i: SizeInt;
begin
  Result := aMileStone;
  If DoPart(pCleanMileStones) then begin
    i := pos('SVN', uppercase(Result));
    if i > 0 then begin
      Delete(Result, i, 3);
      i := pos('(', uppercase(Result));
      if i > 0 then
        Delete(Result, i, 1);
      i := pos(')', uppercase(Result));
      if i > 0 then
        Delete(Result, i, 1);
      Result := Trim(Result);
      if Result <> '' then begin
        // increase to release version
        if Result[Length(Result)] in ['1', '3', '5', '7'] then begin
          Result[Length(Result)] := chr(ord(Result[Length(Result)])+1);
        end
        else
        if (Result[Length(Result)] in ['9']) and (Length(Result) > 1) then begin
          if Result[Length(Result)-1] in ['0'..'9'] then begin
            Result[Length(Result)-1] := chr(ord(Result[Length(Result)-1])+1);
            Result[Length(Result)] := '0';
          end
          else begin
            Result[Length(Result)] := '1';
            Result := Result + '0';
          end
        end
      end;
    end;
  end;
end;

class function TMantisGitlabConverter.EmptyValue(aValue : String) : Boolean;

begin
  aValue:=Trim(aValue);
  Result:=(aValue='') or (aValue='-') or (aValue='?');
end;

procedure TMantisGitlabConverter.CollectVersionStrings(aVersions,
  aProdVersions: TStrings);

Const
  SQLSelect = 'select f.possible_values from '+
              ' mantis_custom_field_project_table fp '+
              ' inner join mantis_custom_field_table f on (fp.field_id=f.id) '+
              ' where  (fp.project_id=:ID)  and (f.name=:Name)';

  SQLSelect2 =
              'select b.fixed_in_version as version '+
              '  from mantis_bug_table b where (b.project_id=:ID) '+
              'union '+
              'select b.version '+
              '  from mantis_bug_table b '+
              'where '+
              '  b.project_id=:ID '+
              'order by 1;';

  SQLSelect3 = 'select distinct version from mantis_project_version_table where project_id=:ID';


Var
  srcLabels : TJSONArray;
  SrcVersions : TJSONArray;
  Src : TJSONObject;
  Values : String;
  Value : String;
  I : Integer;

begin
  DoLog('Collecting versions');
  src:=nil;
  srcLabels:=Nil;
  srcVersions:=Nil;
  try
    // Custom Target version
    if (FConfig.TargetVersionLabelField<>'') then
      begin
      Src:=FMantis.GetSingleRecord(SQLSelect,['ID',FConfig.MantisProjectID, 'Name',FConfig.TargetVersionLabelField],[]);
      Values:=Src.Get('possible_values','');
      DoLog('Versions: %s',[Values]);
      for value in SplitString(Values,'|') do
        if not EmptyValue(Value) then
          aVersions.Add(Value);
      end;
    // Version/Fix version
    SrcLabels:=FMantis.GetRecordList(SQLSelect2,['ID',FConfig.MantisProjectID],[]);
    For I:=0 to SrcLabels.Count-1 do
      begin
      value:=Trim(SrcLabels.Objects[i].get('version',''));
      if not EmptyValue(Value) then
        aVersions.Add(Value);
      end;
    SrcVersions:=FMantis.GetRecordList(SQLSelect3,['ID',FConfig.MantisProjectID],[]);
    For I:=0 to SrcVersions.Count-1 do
      begin
      value:=Trim(SrcVersions.Objects[i].get('version',''));
      if not EmptyValue(Value) then
        aProdVersions.Add(Value);
      end;
    DoLog('Collected %d distinct version strings',[aVersions.Count]);
  finally
    Src.Free;
    SrcLabels.Free;
    SrcVersions.Free;
  end;
end;

function TMantisGitlabConverter.CreateLabel(const aValue, aPrefix,  aDescrPrefix, aColor: string; Unscoped : boolean = false): Boolean;

var
  Dest : TJSONObject;
  aName : string;
  ID : Int64;

begin
  Result:=False;
  try
    if Unscoped then
      aName:=aPrefix+': '+aValue
    else
      aName:=aPrefix+'::'+aValue;
    Dest:=TJSONObject.Create(['name',aName,'description',Format(aDescrPrefix,[aValue]),'color',aColor]);
    try
      ID:=FGitLab.CreateResource(GetProjectResourceURL('labels'),Dest);
      DoLog('label "%s" created with ID %d',[aName,ID]);
      Result:=True;
    finally
      Dest.Free;
    end;
  Except
    On E : exception do
      DoLog('Failed to create label "%s", exception %s : %s',[E.ClassName,E.Message]);
  end;
end;


procedure TMantisGitlabConverter.ConvertVersionLabels;


Var
  Value : string;
  aCount : Integer;

begin
  aCount:=0;
  DoLog('Converting %d version labels',[Versions.Count]);
  for Value in Versions do
    begin
    if CreateLabel(Value,sLabelPrefixFixVersion, 'Fix version: %s', FixVersionColor) then
      inc(aCount);
    if CreateLabel(Value,sLabelPrefixTargetVersion, 'Target version: %s', TargetVersionColor) then
      inc(aCount);
    end;
  for Value in ProdVersions do
    begin
    if CreateLabel(Value,sLabelPrefixVersion, 'Version: %s', VersionColor) then
      inc(aCount);
    end;
  DoLog('Created %d version labels from %d version',[aCount,Versions.Count]);
end;

procedure TMantisGitlabConverter.ConvertVersionMilestones;

Const
  SQLSelect = 'select * from mantis_project_version_table where (project_id=:ID) order by date_order desc';

Var
  SrcVersions : TJSONArray;
  aObj,Dest : TJSONObject;
  S,Value : String;
  released,obsolete : boolean;
  aDate,aCount,ID : Int64;
  I : Integer;
  seen: TStringList;

begin
  SrcVersions:=FMantis.GetRecordList(SQLSelect,['ID',FConfig.MantisProjectID],[]);
  seen := TStringList.Create;
  seen.Sorted := True;
  try
    aCount:=0;
    DoLog('Converting %d versions to milestones',[SrcVersions.Count]);
    For I:=0 to SrcVersions.Count-1 do
      if SrcVersions[i].JSONType=jtObject then
        begin
        aObj:=SrcVersions.Objects[i];
        Value:=CleanMileStoneName(aObj.Get('version',''));
        aDate:=aObj.Get('date_order',0);
        released:=aObj.Get('released',false);
        Obsolete:=aObj.Get('obsolete',false);
        if (Value<>'') and (seen.IndexOf(Value) < 0) then
          begin
          seen.Add(Value);
          S:='Version '+Value;
          Dest:=TJSONObject.Create(['title',S,'description','Release version: '+Value]);
          try
            if aDate<>0 then
               Dest.Add('due_date',FormatDateTime('yyyy"-"mm"-"dd',UnixToDateTime(aDate,False)));
            ID:=FGitLab.CreateResource(GetProjectResourceURL('milestones'),Dest);
            DoLog('Milestone "%s" created with ID %d',[Value,ID]);
            FMileStoneMap.Add(S+'='+IntToStr(ID));
            if obsolete or released then
              begin
              dest.add('state_event','close');
              ID:=FGitLab.UpdateResource(GetProjectResourceURL('milestones/'+IntToStr(ID)),Dest);
              end;
            inc(aCount);
          finally
            Dest.Free;
          end;
          end;
        end;
    DoLog('Successfully converted %d versions to milestones.',[aCount]);
  finally
    SrcVersions.Free;
    seen.Free;
  end;
end;

procedure TMantisGitlabConverter.CreateMileStoneMap;

Var
  List : TJSONArray;
  Obj : TJSONObject;
  S : String;
  I,acount,apage : Integer;
  aID : Int64;

begin
  DoLog('No milestones found, Building milestone map from gitlab.');
  aPage:=0;
  repeat
    List:=FGitlab.GetResourceList(Format('projects/%d/milestones',[Config.GitlabProjectID]),['per_page','100','page',inttostr(apage)]);
    try
      aCount:=List.Count;
      For I:=0 to List.Count-1 do
        begin
        Obj:=List.Objects[i];
        S:=Obj.Get('title','');
        aId:=Obj.Get('id',0);
        if (S<>'') and (aID<>0) then
          FMileStoneMap.Add(S+'='+IntToStr(aID));
        end;
    finally
      List.Free;
    end;
    inc(aPage)
  until (aCount<100);
  DoLog('Built milestone map with %d items',[FMileStoneMap.Count]);
end;

function TMantisGitlabConverter.MapUser(const aName, aRealName, aEmail: string
  ): String;

begin
  Result:='';
  if (aName<>'') then
    Result:=FUserMap.Items[aName];
  if (Result='') and (aRealName<>'') then
    Result:=FUserMap.Items[aRealName];
  if (Result='') and (aEmail<>'') then
    Result:=FUserMap.Items[aEmail];
  if Result='' then
    DoLog('Could not map (login: "%s", Real: "%s", Email: "%s")',[aName,aRealName,aEmail]);
end;

function TMantisGitlabConverter.MapMileStone(aVersion: string): Int64;
begin
  Result:=-1;
  aVersion := Dash2Empty(CleanMileStoneName(aVersion));
  if aVersion='' then
    exit;
  Result:=StrToInt64Def(FMilestoneMap.Values['Version '+aVersion],-1);
  if Result=-1 then
    Result:=StrToInt64Def(FMilestoneMap.Values[aVersion],-1);
  if Result=-1 then
    DoLog('Failed to find milestone "%s" in %d milestones',[aVersion,FMileStoneMap.Count]);
end;

function TMantisGitlabConverter.GetBugLabels(aSrc: TJSONObject;
  aMileStone: int64): String;

Var
  SL : TStringList;
  S, ExtraLabel, Target, Fixed : string;

  Procedure CreateLabelFromMap(aMap : TStrings; aPrefix: String; aValue : Integer);

  Var
    Idx : Integer;

  begin
    if aValue=0 then exit;
    Idx:=aMap.IndexOfName(IntToStr(aValue));
    if Idx<>-1 then
       Sl.Add(aPrefix+'::'+aMap.ValueFromIndex[Idx]);
  end;

  Procedure MaybeAdd(aPrefix,aValue : string);
  begin
    if not EmptyValue(aValue) then
      SL.Add(aPrefix+'::'+aValue);
  end;

begin
  SL:=TStringList.Create;
  try
    SL.Delimiter:=',';
    SL.StrictDelimiter:=True;
    MaybeAdd(SLabelPrefixVersion,aSrc.get('version',''));
    Fixed := aSrc.get('fixed_in_version','');
    if (MapMileStone(Fixed) <> aMileStone) or not DoPart(pSkipLabelEqMilestone) then
      MaybeAdd(SLabelPrefixFixVersion, Fixed);
    Target:=aSrc.get('target_version','');
    if EmptyValue(Target) then
      Target:=aSrc.get('target_extra','');
    if (MapMileStone(Target) <> aMileStone) or not DoPart(pSkipLabelEqMilestone) then
      MaybeAdd(SLabelPrefixTargetVersion,Target);
    MaybeAdd(SLabelPrefixCategory,aSrc.get('category_name',''));
    if not DoPart(pSkipReproducibility) then
      CreateLabelFromMap(FReproducabilityMap,SlabelPrefixReproducibility,aSrc.Get('reproducibility',0));
    CreateLabelFromMap(FSeverityMap,SlabelPrefixSeverity,aSrc.Get('severity',0));
    CreateLabelFromMap(FStatusMap,SlabelPrefixStatus,aSrc.get('status',0));
    if not DoPart(pPriorityAsWeight) then
      CreateLabelFromMap(FPriorityMap,SlabelPrefixPriority,aSrc.Get('priority',0));
    CreateLabelFromMap(FResolutionMap,SlabelPrefixResolution,aSrc.get('resolution',0));
    ExtraLabel:=aSrc.Get('extra_label','');
    if not EmptyValue(ExtraLabel) then
      // Scoped can only have a single value...
      if Not FConfig.ExtraFieldUnscoped then
        SL.Add(FConfig.ExtraFieldLabel+':: '+ExtraLabel)
      else
        begin
        for S in SplitString(ExtraLabel,'|') do
          if Not EmptyValue(S) then
            SL.Add(FConfig.ExtraFieldLabel+': '+S);
        end;
    Result:=SL.DelimitedText;
  finally
    SL.Free;
  end;
end;

function TMantisGitlabConverter.ConvertRevToHash(AText: String): String;
var
  i, j: Integer;
  rev, sha1: String;
begin
  Result := AText;
  i := 1;

  while (i <= Length(Result)) and not(Result[i] in ['0'..'9']) do
    inc(i);

  while (i <= Length(Result)) do begin
    j := i;
    while (j <= Length(Result)) and (Result[j] in ['0'..'9']) do
      inc(j);

    if j-i >= 2 then begin
      rev := copy(Result, i, j-i);
      sha1 := FRevisionToHash.Items[rev];
      if sha1 <> '' then begin
        insert(' (#'+sha1+')', Result, j);
        inc(j, Length(sha1)+4);
      end;
    end;

    i := j;
    while (i <= Length(Result)) and not(Result[i] in ['0'..'9']) do
      inc(i);
  end;
end;

function TMantisGitlabConverter.GetMonitors(aBugID : Int64) : String;

Const
  SQLSelect = 'select u.username, u.realname, u.email '+
              'from '+
              '  mantis_bug_monitor_table bm '+
              '  left join mantis_user_table u on bm.user_id=u.id '+
              'where '+
              ' (bm.bug_id=:ID)';
var
  Arr : TJSONArray;
  I : Integer;
  Src : TJSONObject;
  UN,RRN,UEM,UGN,Ref : String;

begin
  Result:='';
  Arr:=FMantis.GetRecordList(SQLSelect,['ID',aBugID],[]);
  try
    DoLog('Got %d monitors for bug ID %d',[Arr.Count,aBugID]);
    For I:=0 to Arr.Count-1 do
      begin
      Src:=Arr.Objects[i];
      UN:=Src.Get('username','');
      RRN:=Src.Get('realname','');
      UEM:=Src.Get('email','');
      UGN:=MapUser(UN,RRN,UEM);
      if RRN='' then
        RRN:=UN;
      if UGN<>'' then
        Ref:='» @'+UGN+' ('+RRN+')'
      else
        Ref:='» '+UN+' ('+RRN+')';
      if Result<>'' then
        Result:=Result+', ';
      Result:=Result+Ref;
      end;
  finally
    Arr.Free;
  end;
end;

function TMantisGitlabConverter.GetDescription(aSrc: TJSONObject;
  Flags: TDescriptionFlags; const aReporterGitlabUser, aAssignedGitlabUser : String): String;

Var
  Content : TStrings;
  Monitors, Rep, Target, Extra : string;

  Procedure EmptyLine;
  begin
    Content.Add('');
  end;

  Procedure MaybeAdd(aText,aHeader : String);
  Var
    SL : TStrings;

  begin
    if aText='' then exit;
    Content.Add('## '+aHeader+':');
    EmptyLine;
    SL:=TStringList.Create;
    try
      SL.Text:=aText;
      Content.AddStrings(SL);
    finally
      SL.Free;
    end;
    EmptyLine;
  end;

  Procedure MaybeAddValue(aName,aText: String);

  begin
    if (aText<>'') then
      Content.Add('- **'+aName+':** '+aText);
  end;

begin
  Content:=TStringList.Create;
  try
    With aSrc do
      begin
      Content.Add('');
      Rep:=Get('reporter_username','');
      if aReporterGitlabUser<>'' then
        Rep:=Rep+' @'+aReporterGitlabUser;
      Content.Add('<h3><details><summary>Original Reporter info from Mantis: <small>'+Rep+'</small></summary><small>');
      Content.Add('');
      Content.Add('- **Reporter name:** '+Get('reporter_realname',''));
      if dfSubmitterEmail in flags then
        Content.Add('- **Reporter email:** '+Get('reporter_email',''));
      Content.Add('</small></details></h3>');
      Content.Add('');
      MaybeAdd(FormatUserText(Get('description','')),'Description');
      MaybeAdd(FormatUserText(Get('steps_to_reproduce','')),'Steps to reproduce');
      MaybeAdd(FormatUserText(Get('additional_information','')),'Additional information');
      Content.Add('## Mantis conversion info:');
      EmptyLine;
      Content.Add('- **Mantis ID:** '+IntToStr(Get('id',Int64(0))));
      if dfAssignee in Flags then
        begin
        if aAssignedGitlabUser<>'' then
          begin
          Writeln('Referencing assigned gitlab user: ',aAssignedGitlabUser);
          Content.Add('- **Assignee Gitlab user:** @'+aAssignedGitlabUser);
          end;
        Content.Add('- **Assignee Mantis username:** '+Get('assigned_username',''));
        Content.Add('- **Assignee name:** '+Get('assigned_realname',''));
        if (dfAssigneeEmail in Flags) then
           Content.Add('- **Assignee email:** '+Get('assigned_email',''));
        end;
      MaybeAddValue('OS',Get('os',''));
      MaybeAddValue('OS Build',Get('os_build',''));
      MaybeAddValue('Build',Get('build',''));
      MaybeAddValue('Platform',Get('platform',''));
      MaybeAddValue('Version',Get('version',''));
      MaybeAddValue('Fixed in version',Get('fixed_in_version',''));
      MaybeAddValue('Fixed in revision',ConvertRevToHash(Get('fixed_in_revision','')));
      MaybeAddValue('Monitored by',getMonitors(get('id',Int64(0))));
      Target:=Dash2Empty(aSrc.get('target_version',''));
      Extra:=Dash2Empty(aSrc.get('target_extra',''));
      if Target='' then
        Target:=Extra;
      MaybeAddValue('Target version', Target);
      if (Target <> Extra) and (Config.TargetVersionLabelField <> '') then
        MaybeAddValue(Config.TargetVersionLabelField, Extra);
      Result:=Content.text;
      end;
  finally
    Content.Free;
  end;
end;

function TMantisGitlabConverter.GetGitlabUserNameFromID(aUserID : Int64): String;

Var
  Obj : TJSONObject;
  Idx : PtrInt;

begin
  Result:='';
  Obj:=FGitlab.GetSingleResource('users/'+IntToStr(aUserID),[]);
  if Obj=Nil then
    DoLog('Failed to retrieve user %d',[aUserID])
  else
    begin
    Result:=Obj.Get('username','');
    if Result<>'' then
      begin
      // Add to user map
      Idx:=FGitlabUserIDMap.IndexOf(Result);
      if Idx<0 then
        FGitlabUserIDMap.AddObject(Result,TObject(Idx));
      end;
    end;
end;

function TMantisGitlabConverter.MapGitlabUserID(aUser: string): PtrInt;

Var
  Idx : Integer;
  Arr : TJSONArray;
  Obj : TJSONObject;

begin
  Obj:=Nil;
  Idx:=FGitlabUserIDMap.IndexOf(aUser);
  if Idx>=0 then
    Result:=PtrInt(FGitlabUserIDMap.Objects[Idx])
  else
    begin
    Arr:=FGitlab.GetResourceList('users',['username',aUser]);
    try
      if Arr.Count=1 then
        Obj:=Arr.Objects[0];
      if Obj<>Nil then
        begin
        Result:=Obj.Get('id',Int64(-1));
        if (Result<>-1) then
          begin
          FGitlabUserIDMap.AddObject(aUser,TObject(Pointer(Result)));
          Dolog('Got id %d for user %s',[Result,aUser]);
          end;
        end;
    finally
      arr.Free;
    end;
    end;
end;

procedure TMantisGitlabConverter.CopyBugData(aSrc, aDest : TJSONObject);

Var
  aOldID,UID : Integer;
  Flags : TDescriptionFlags;
  aMileStone,TimeStamp : int64;
  aReporterUser,aAssignedUser,Target,Fixed : String;

begin
  Flags:=[];
  aAssignedUser:=MapUser(aSrc.Get('assigned_username',''), aSrc.Get('assigned_realname',''), aSrc.Get('assigned_email',''));
  aReporterUser:=MapUser(aSrc.Get('reporter_username',''), aSrc.Get('reporter_realname',''), aSrc.Get('reporter_email',''));
  UID:=StrToIntDef(aAssignedUser,-1);
  if UID=-1 then
    UID:=MapGitlabUserID(aAssignedUser);
  if UID=-1 then
    Include(Flags,dfAssignee)
  else
    aDest.Add('assignee_id',UID);
  // aDest.Add('assignee_ids',[]);
  aDest.Add('confidential',(aSrc.Get('view_state',0)<>10));
  TimeStamp:=aSrc.Get('date_submitted',Int64(0));
  if TimeStamp>1000 then
    aDest.Add('created_at',DateToISO8601(UnixToDateTime(TimeStamp,True),true)); // date
  aDest.Add('description',GetDescription(aSrc,Flags,aReporterUser,aAssignedUser));
  // aDest.Add('discussion_to_resolve','');
  TimeStamp:=aSrc.Get('due_date',Int64(0));
  if TimeStamp>1000 then
    aDest.Add('due_date',DateToISO8601(UnixToDateTime(TimeStamp,True),true)); // date
  // aDest.Add('epic_id',0);
  // aDest.Add('epic_iid',0);
  aOldID:=aSrc.get('id',Int64(0));
  if aOldID>0 then
    aDest.Add('iid',aOldID);
  aDest.Add('issue_type','issue'); // One of issue, incident, or test_case. Default is issue.
  aDest.Add('title',aSrc.Get('summary',''));
  // Convert target to milestone.
  aMileStone := 0;
  Target:=Dash2Empty(aSrc.get('target_version',''));
  if Target='' then
    Target:=Dash2Empty(aSrc.get('target_extra',''));
  Fixed := Dash2Empty(aSrc.get('fixed_in_version',''));
  if (Target<>'') or (Fixed<>'') then
    begin
    aMileStone:=MapMileStone(Fixed);
    if aMileStone<0 then
      aMileStone:=MapMileStone(Target);
    if aMileStone>=0 then
      aDest.Add('milestone_id',aMileStone);
    end;
  aDest.Add('labels',GetBugLabels(aSrc, aMileStone)); //  Comma-separated

  if DoPart(pPriorityAsWeight) and (aSrc.Get('priority',0) > 30) then
    aDest.Add('weight', max(0, aSrc.Get('priority',0)-30) * 10);
end;

procedure TMantisGitlabConverter.ConvertBugFiles(aOldBugID, aNewBugID : Int64) ;

Const
  SQLSelect = 'select b.diskfile,b.filename, b.filesize, b.date_added, b.user_id, '+
              ' u.username, u.realname, u.email '+
              ' from '+
              '  mantis_bug_file_table b '+
              ' left join mantis_user_table u on (b.user_id=u.id) '+
              ' where (bug_id=:ID)';


Var
  Arr : TJSONarray;
  Src,Dest : TJSONObject;
  MD,Descr,NoteMarkDown, aTime, LocalFileName,RemoteFileName, aUser, aUserReal: String;
  I,aSize,aCount : Integer;
  aNewNoteID, aDate,aLastDate : Int64;

begin
  Dest:=Nil;
  aLastDate:=0;
  aCount:=0;
  NoteMarkDown:='## Attached files:'+sLineBreak+sLineBreak;
  Arr:=FMantis.GetRecordList(SQLSelect,['ID',aOldBugID],[]);
  try
    DoLog('Uploading %d files for bug ID %d',[Arr.Count,aOldBugID]);
    For I:=0 to Arr.Count-1 do
      begin
      Src:=Arr.Objects[i];
      LocalFileName:=IncludeTrailingPathDelimiter(FConfig.BugFileDir)+Src.Get('diskfile','');
      RemoteFileName:=Src.Get('filename','');
      aSize:=Src.Get('filesize',0);
      aUser:=Src.Get('username','');
      aUserReal:=Src.Get('realname','');
      aDate:=Src.Get('date_added',0);
      if aDate>aLastDate then
        aLastDate:=aDate;
      if aDate>1000 then
        aTime:=FormatDateTime('yyyy"-"mm"-"dd',UnixToDateTime(aDate,False))
      else
        aTime:='?';
      Descr:=Format(' (Size: %d bytes, user: %s (%s), date: %s)',[aSize,aUser,aUserReal,aTime]);
      if not FileExists(LocalFileName) then
        DoLog('Local file "%s" of attached file %s does not exist',[LocalFileName,RemoteFileName])
      else
        begin
        MD:=FGitlab.UploadFile(GetProjectResourceURL('uploads'), LocalFileName,RemoteFileName);
        NoteMarkDown:=NoteMarkDown+sLineBreak+'* '+MD+' '+Descr;
        Inc(aCount);
        end;
      end;
  finally
    Arr.Free;
  end;
  if aCount=0 then
    exit;
  NoteMarkDown:=NoteMarkDown+sLineBreak;
  Dest:=TJSONObject.Create(['body',NoteMarkDown,'issue_iid',aOldBugID]);
  try
    if aLastDate>1000 then
      begin
      aTime:=DateToISO8601(UnixToDateTime(aDate,True),true);
      Dest.Add('created_at',aDate);
      end;
    try
      aNewNoteID:=FGitlab.CreateResource(GetProjectResourceURL(Format('issues/%d/notes',[aOldBugID])),Dest);
      Dolog('Added uploaded files note %d to %d',[aOldBugID,aNewNoteID]);
    except
      On E : Exception do
        begin
        DoLog('Failed to create uploaded file note for issue %d to Gitlab (%s): %s',[aOldBugID,E.ClassName,E.Message])
        end;
    end;
  finally
    Dest.Free;
  end;
end;

procedure TMantisGitlabConverter.ConvertBugLinks;

Const
  SQLSelect = 'select ' +
              ' r.*, bsrc.project_id as src_project_id, bdest.project_id as dest_project_id '+
              ' from mantis_bug_relationship_table r '+
              ' left join mantis_bug_table bsrc on (r.source_bug_id=bsrc.id) '+
              ' left join mantis_bug_table bdest on (r.destination_bug_id=bdest.id) '+
              ' where (bsrc.project_id=:ID)';


Var
  Arr : TJSONarray;
  Src : TJSONObject;
  Tmp,Rel,SrcID,TargetID,I,DupID,SrcProjectID,DestProjectID,DestMappedID : Integer;
  linktype : string;

begin
  Arr:=FMantis.GetRecordList(SQLSelect,['ID',FConfig.MantisProjectID],[]);
  try
    DoLog('Converting %d links between bugs',[Arr.Count]);
    For I:=0 to Arr.Count-1 do
      begin
      Src:=Arr.Objects[i];
      SrcID:=Src.get('source_bug_id',0);
      TargetID:=Src.get('destination_bug_id',0);
      Rel:=Src.get('relationship_type',0);
      SrcProjectID:=Src.get('src_project_id',0);
      DestProjectID:=Src.get('dest_project_id',0);
      DupID:=0;
      if (Rel=BUG_REL_NONE) then
        Continue;
      if (SrcProjectID=DestProjectID) then
        DestMappedID:=FConfig.MantisProjectID
      else
        begin
        DestMappedID:=MapProjectID(DestProjectID);
        if DestMappedID<=0 then
          begin
          DoLog('Skipping link from %d to %d in unknown project: %d',[SrcID,TargetID,DestProjectID]);
          Continue;
          end;
        end;
      Case Rel of
        BUG_REL_ANY,
        BUG_RELATED :  Linktype:=SRelatesTo;
        BUG_DUPLICATE :
          begin
          DupID:=SrcID;
          Linktype:=SRelatesTo;
          end;
        BUG_HAS_DUPLICATE :
          begin
          DupID:=TargetID;
          Linktype:=SRelatesTo;
          Tmp:=TargetID;
          TargetID:=SrcID;
          SrcID:=Tmp;
          end;
        BUG_DEPENDANT : Linktype:= SBlockedBy;
        BUG_BLOCKS : Linktype:= SBlocks;
      end;
      try
        CreateLink(SrcID,TargetID,DestMappedID,DupID,LinkType);
      except
        on E : Exception do
          DoLog('Failed to link issue ID %d to issue %d, exception: %s : %s',[SrcID,TargetID,E.ClassName,E.Message]);
      end;
      end;
  finally
    Arr.Free;
  end;
end;

procedure TMantisGitlabConverter.CreateLink(SrcID, TargetID, TargetProjectID,
  DupID: Integer; const LinkType: string);

Var
  Dest : TJSONObject;
  Descr :string;
  aNewNoteID : Integer;

begin
  Dest:=TJSONObject.Create(['target_project_id',TargetProjectID,'target_issue_iid',TargetID,'link_type',LinkType]);
  try
    FGitLab.CreateResourceRaw(GetProjectResourceURL(Format('issues/%d/links',[SrcID])),Dest);
    DoLog('Marked issue %d as related to %d',[SrcID,TargetID])
  finally
    Dest.Free;
  end;
  if (DupID>0) then
    begin
    Descr:='Duplicate of #'+IntToStr(TargetID);
    Dest:=TJSONObject.Create(['body',descr,'issue_iid',SrcID]);
    try
      aNewNoteID:=FGitlab.CreateResource(GetProjectResourceURL(Format('issues/%d/notes',[SrcID])),Dest);
      DoLog('Added note %d to issue %d marking it as duplicate of %d',[aNewNoteID,SrcID,TargetID]);
    finally
      Dest.Free;
    end;
    end;
end;

procedure TMantisGitlabConverter.ConvertBugNotes(aOldBugID, aNewBugID : Int64) ;

Const
  SQLSelect = 'select '+
              ' u.realname as submitter_realname, u.email as submitter_email, u.username as submitter_username, '+
              ' bn.id, bn.date_submitted, bnt.note, bn.view_state '+
              ' from '+
              ' mantis_bugnote_table bn '+
              ' left join mantis_bugnote_text_table bnt on (bn.bugnote_text_id=bnt.id) '+
              ' left join mantis_user_table u on (u.id=bn.reporter_id) '+
              ' where '+
              '  (bn.bug_id=:ID) %s ' +
              '  order by bn.date_submitted ';

  SQLOnlyPublic = ' and (bn.view_state=10) ';

Var
  Arr : TJSONarray;
  Src,Dest : TJSONObject;
  I : Integer;
  aOldID,aNewNoteID,aTime : Int64;
  ref,ref2,gitsubmitter,submitter,realname, aDate,SQL,aWhere,Descr : String;
  conf : Boolean;

begin
  if DoPart(pPrivateComments) then
    aWhere:=''
  else
    aWhere:=SQLOnlyPublic;
  SQL:=Format(SQLSelect,[aWhere]);
  Arr:=FMantis.GetRecordList(SQL,['ID',aOldBugID],[]);
  try
    DoLog('Converting %d notes for bug id %d',[Arr.Count, aOldBugID]);
    For I:=0 to Arr.Count-1 do
      begin
      Src:=Arr.Objects[i];
      aOldID:=Src.Get('id',Int64(0));
      conf:=Src.Get('view_state',0)<>10;
      submitter:=Src.Get('submitter_username','');
      gitsubmitter:=MapUser(submitter,'','');
      realname:=Src.Get('submitter_realname','');
      if RealName='' then
        RealName:=submitter;
      Ref:=RealName;
      Ref2:=submitter;
      if gitsubmitter<>'' then
        begin
        Ref:=Ref+' @'+gitsubmitter;
        Ref2:=Ref2+' @'+gitsubmitter;
        end;
      Descr:='<h3><details><summary>Converted from Mantis. <small>Note by '+ref+'</small></summary><small><small><br/>'+sLineBreak+sLineBreak;
      Descr:=Descr+'- **Mantis submitter username:** '+ref2+sLineBreak;
      Descr:=Descr+'- **Mantis submitter real name:** '+realname+sLineBreak;
      if Conf then
        Descr:=Descr+'- **Mantis submitter email:** '+Src.Get('submitter_email','')+sLineBreak;
      Descr:=Descr+'</small></small><hr /></details></h3>';
      //Descr:=Descr+sLineBreak+sLineBreak;
      //Descr:=Descr+'## Mantis text';
      Descr:=Descr+sLineBreak+sLineBreak;
      Descr:=Descr+FormatUserText(Src.get('note',''));
      Dest:=TJSONObject.Create(['body',descr,'issue_iid',aOldBugID]);
      try
        if Conf then
          Dest.Add('confidential',Conf);
        aTime:=Src.Get('date_submitted',Int64(0));
        if aTime>1000 then
          begin
          aDate:=DateToISO8601(UnixToDateTime(aTime,True),true);
          Dest.Add('created_at',aDate);
          end;
        try
          aNewNoteID:=FGitlab.CreateResource(GetProjectResourceURL(Format('issues/%d/notes',[aOldBugID])),Dest);
          Dolog('Converted note %d to %d',[aOldID,aNewNoteID]);
        except
          On E : Exception do
            begin
            DoLog('Failed to convert note %d to Gitlab (%s): %s',[aOldID,E.ClassName,E.Message])
            end;
        end;
      finally
        Dest.Free;
      end;
      Sleep(SleepInterval);
      end;
  finally
    arr.Free;
  end;
end;

class function TMantisGitlabConverter.ToSQLList(const aList: String): string;

Const
  Seps = [',',';'];

Var
  I : integer;
  S : String;

begin
  Result:='';
  For I:=1 to WordCount(aList,Seps) do
    begin
    S:=Trim(ExtractWord(I,aList,Seps));
    if Result<>'' then
      Result:=Result+' , ';
    Result:=Result+''''+StringReplace(S,'''','''''',[rfReplaceAll])+'''';
    end;
end;

procedure TMantisGitlabConverter.ConvertBugs;

Const
   SQLSelectBugs =
              ' with buglist as ( '+sLineBreak+
              ' select '+sLineBreak+
              '   u.username as reporter_username,u.realname as reporter_realname, u.email as reporter_email, '+sLineBreak+
              '   h.username as assigned_username, h.realname as assigned_realname, h.email as assigned_email, '+sLineBreak+
              '   b.os, b.os_build,b.platform, b.version, b.fixed_in_version, b.build, b.date_submitted, '+sLineBreak+
              '   b.due_date, b.priority, b.summary, b.resolution, b.reproducibility, b.status, b.severity, '+sLineBreak+
              '   b.id, b.target_version , b.view_state, '+sLineBreak+
              '   bt.description, bt.steps_to_reproduce, bt.additional_information , '+sLineBreak+
              '   c.name as category_name '+sLineBreak+
              ' from '+sLineBreak+
              '     mantis_bug_table b '+sLineBreak+
              '     left join mantis_user_table u on b.reporter_id=u.id '+sLineBreak+
              '     left join mantis_bug_text_table bt on b.bug_text_id=bt.id '+sLineBreak+
              '     left join mantis_category_table c on b.category_id=c.id '+sLineBreak+
              '     left join mantis_user_table h on b.handler_id=h.id '+sLineBreak+
              '  where '+sLineBreak+
              '     (b.id>=:StartID) and (b.project_id=:ID) '+sLineBreak+
              '     %s'+sLineBreak+
              '  order by b.id '+sLineBreak+
              '   %s '+sLineBreak+
              ' ) '+sLineBreak;

   SQLSelectBugsExtra =
             ' select bcf.value as target_extra, buglist.* from buglist '+sLineBreak+
             '  left join '+sLineBreak+
             '    (mantis_custom_field_string_table bcf '+sLineBreak+
             '     inner join mantis_custom_field_table cf '+sLineBreak+
             '         on (cf.id=bcf.field_id) and (cf.name=''%s'') '+sLineBreak+
             '     ) on (buglist.id=bcf.bug_id) '+sLineBreak;


   SQLSelectBugsRaw =
             ' select ''::varchar(1) as target_extra, buglist.* from buglist '+sLineBreak;

   SQLSelectBugsFixedInRevision
             = 'with bugswithfixedin as ('+sLineBreak+
                ' %s '+sLineBreak+
                ')'+sLineBreak+
                ' select bcef2.value as fixed_in_revision, bugswithfixedin.* from bugswithfixedin '+sLineBreak+
                '  left join '+sLineBreak+
                '    (mantis_custom_field_string_table bcef2 '+sLineBreak+
                '     inner join mantis_custom_field_table cef2 '+sLineBreak+
                '         on (cef2.id=bcef2.field_id) and (cef2.name=''%s'') '+sLineBreak+
                '     ) on (bugswithfixedin.id=bcef2.bug_id) '+sLineBreak;

   SQLSelectBugsNoFixedInRevision
                = 'with bugswithfixedin as ('+sLineBreak+
                ' %s '+sLineBreak+
                ')'+sLineBreak+
                ' select '''' as fixed_in_revision, bugswithfixedin.* from bugswithfixedin '+sLineBreak;


   SQLBugsTarget = 'with bugswithtarget as ('+sLineBreak+
             ' %s '+sLineBreak+
             ')'+sLineBreak+
             ' select bcef.value as extra_label, bugswithtarget.* from bugswithtarget '+sLineBreak+
             '  left join '+sLineBreak+
             '    (mantis_custom_field_string_table bcef '+sLineBreak+
             '     inner join mantis_custom_field_table cef '+sLineBreak+
             '         on (cef.id=bcef.field_id) and (cef.name=''%s'') '+sLineBreak+
             '     ) on (bugswithtarget.id=bcef.bug_id) '+sLineBreak;

   SOrderBy = sLineBreak+'order by id'+sLineBreak;

(*
'select '+
               ' u.username as reporter_username,u.realname as reporter_realname, u.email as reporter_email, '+
               ' h.username as assigned_username, h.realname as assigned_realname, h.email as assigned_email, '+
               ' b.id, b.os, b.os_build,b.platform, b.version, b.fixed_in_version, b.build, b.date_submitted, '+
               ' b.due_date, b.priority, b.summary, b.resolution, b.reproducibility, b.status, b.severity, '+
               ' b.view_state, '+
               ' b.target_version , bt.description, bt.steps_to_reproduce, bt.additional_information ,'+
               ' c.name as category_name '+
               ' from '+
               '   mantis_bug_table b '+
               '   left join mantis_user_table u on b.reporter_id=u.id '+
               '   left join mantis_bug_text_table bt on b.bug_text_id=bt.id '+
               '   left join mantis_category_table c on b.category_id=c.id '+
               '   left join mantis_user_table h on b.handler_id=h.id '+
               'where '+
               '   (b.id>=:StartID) and (b.project_id=:ID)';
*)

Var
  nr,aLimit,SQL,awhere : String;
  Arr : TJSONarray;
  Src,Dest : TJSONObject;
  aStatus,I : Integer;
  aOldID,aNewID : Int64;


begin
  if (FMilestoneMap.Count=0) then
    CreateMileStoneMap;
  if FConfig.Count>0 then
    aLimit:=' limit '+IntToStr(FConfig.Count)
  else
    aLimit:='';
  aWhere:='';
  if Trim(FConfig.OnlyCategories)<>'' then
    aWhere:='and (c.name in ('+ToSQLList(FConfig.OnlyCategories)+')) ';
  if Trim(FConfig.ExcludeCategories)<>'' then
    begin
    aWhere:=aWhere+' and not (c.name in ('+ToSQLList(FConfig.ExcludeCategories)+')) ';
    end;
  SQL:=Format(SQLSelectBugs,[awhere,aLimit])+sLineBreak;
  // Add extra Target field if one is supplied.
  if (FConfig.TargetVersionLabelField<>'') then
    SQL:=SQL+Format(SQLSelectBugsExtra,[FConfig.TargetVersionLabelField])
  else
    SQL:=SQL+SQLSelectBugsRaw;
  if (FConfig.FixedInRevisionField<>'') then
    SQL:=Format(SQLSelectBugsFixedInRevision,[SQL,FConfig.FixedInRevisionField])
  else
    SQL:=Format(SQLSelectBugsNoFixedInRevision,[SQL,FConfig.FixedInRevisionField]);
  // Add extra field if one is supplied.
  if (FConfig.ExtraFieldName<>'') then
    SQL:=Format(SQLBugsTarget,[SQL,FConfig.ExtraFieldName]);
  SQL:=SQL+SOrderBy;
  Writeln('SQL : ',SQL);
  Arr:=FMantis.GetRecordList(SQL,['StartID',Config.StartID,'ID',Config.MantisProjectID],[]);
  try
    DoLog('Converting %d bugs',[Arr.Count]);
    // exit;
    For I:=0 to Arr.Count-1 do
      begin
      Nr:=Format('[%d/%d] ',[i+1,Arr.Count]);
      Src:=Arr.Objects[i];
      aOldID:=Src.Get('id',Int64(0));
      DoLog(Nr+'Starting bug ID %d',[aOldID]);
      Dest:=TJSONObject.Create();
      try
        CopyBugData(Src,Dest);
        try
          // FGitlab.Sudo:=Mapuser(Src.Get('reporter_username',''),'','');
          aNewID:=FGitlab.CreateResource(GetProjectResourceURL('issues'),Dest);
          aStatus:=Src.Get('status',0);
          if aStatus in [StatusResolved,StatusClosed] then
            begin
            Dest.Free;
            Dest:=TJSONObject.Create(['state_event', 'close']);
            if aStatus=90 then
               Dest.Add('discussion_locked',true);
            FGitLab.UpdateResource(GetProjectResourceURL(format('issues/%d',[aOldID])),Dest);
            end;
          Sleep(SleepInterval);
          if DoPart(pComments) then
            ConvertBugNotes(aOldID,aNewID);
          if DoPart(pFiles) then
            ConvertBugFiles(aOldID,aNewID);
          DoLog(Nr+'Converted old bug %d to new bug %d,',[aOldID,aNewID]);
        except
          On E : Exception do
            DoLog(Nr+' Error converting old bug %d (%s) : %s',[aOldID,E.ClassName,E.Message]);
        end;
      finally
        Dest.Free;
      end;
      end;
    DoLog('Last converted bug ID: old %d (=iid) to new id: %d',[aOldID,aNewID]);
  finally
    Arr.Free;
  end;
end;

procedure TMantisGitlabConverter.LoadProjectMap;

Var
  L : TStringList;
  aCount,I,NID,VID : Integer;
  N,V : String;

begin
  if (FConfig.UserMapFile='') then
    Exit;
  L:=TStringList.Create;
  try
    aCount:=0;
    DoLog('Loading project map from file : %s',[FConfig.ProjectMapFile]);
    L.LoadFromFile(FConfig.ProjectMapFile);
    For I:=0 to L.Count-1 do
      begin
      L.GetNameValue(I,N,V);
      if TryStrToInt(N,NID) and TryStrToInt(V,VID) then
        if FuserMap.Find(IntToStr(NID))=nil then
          begin
          FUserMap.Add(IntToStr(NID),IntToStr(VID));
          Inc(aCount);
          end;
      end;
    DoLog('Loaded %d project maps from file : %s',[aCount,FConfig.ProjectMapFile]);
  finally
    L.Free;
  end;
end;

procedure TMantisGitlabConverter.LoadRevisionMap;

Var
  L : TStrings;
  I : Integer;
  rev,hash : string;

begin
  if (FConfig.RevisionMapFile='') then
    Exit;
  L:=Nil;
  try
    L:=TstringList.Create;
    L.LoadFromFile(FConfig.RevisionMapFile);
    DoLog('Loading revision map from %s',[FConfig.RevisionMapFile]);
    For I:=0 to L.Count-1 do
      begin
      L.GetNameValue(I,Rev,hash);
      if (Rev<>'') then
        if (FRevisionToHash.Find(rev)=nil) then
          FRevisionToHash.Add(Rev,Hash)
        else
          DoLog('Double revison %s -> %s (New: %s)',[Rev,FRevisionToHash.Items[rev],Hash]);
      end;
    DoLog('Loaded %d revisions',[FRevisionToHash.Count]);
  finally
    L.Free;
  end;
end;

procedure TMantisGitlabConverter.LoadUserMap;

Var
  L : TStringList;
  aCount,I : Integer;
  N,V : String;

begin
  if (FConfig.UserMapFile='') then
    Exit;
  L:=TStringList.Create;
  try
    aCount:=0;
    DoLog('Loading user map from file : %s',[FConfig.UserMapFile]);
    L.LoadFromFile(FConfig.UserMapFile);
    For I:=0 to L.Count-1 do
      begin
      L.GetNameValue(I,N,V);
      if (N<>'') and (V<>'') then
        if FuserMap.Find(N)=nil then
          begin
          DoLog('Adding mantis user map "%s" -> "%s"',[N,V]);
          FUserMap.Add(N,V);
          Inc(aCount);
          end;
      end;
    DoLog('Loaded %d user maps from file : %s',[aCount,FConfig.UserMapFile]);
  finally
    L.Free;
  end;
end;

procedure TMantisGitlabConverter.LoadGitlabUserMap;

Var
  L : TStringList;
  I : Integer;
  N,V : String;
  Idx : PtrInt;

begin
  if (Config.GitlabUserIDMapFile='') then
    exit;
  if Not FileExists(Config.GitlabUserIDMapFile) then
    begin
    DoLog('File with gitlab user->ID map does not exist: %s',[Config.GitlabUserIDMapFile]);
    exit;
    end;
  DoLog('Loading gitlab user->ID map file %s',[Config.GitlabUserIDMapFile]);
  L:=TStringList.Create;
  try
    L.LoadFromFile(Config.GitlabUserIDMapFile);
    FGitlabUserIDMap.Capacity:=L.Count*2;
    For I:=0 to L.Count-1 do
      begin
      L.GetNameValue(I,N,V);
      Idx:=StrToIntDef(V,-1);
      if Idx<>-1 then
        FGitlabUserIDMap.AddObject(N,TObject(Idx));
      end;
    DoLog('Loading gitlab user->ID map file');
  finally
    L.Free;
  end;
end;

procedure TMantisGitlabConverter.LoadMantisUserMap;

const
  SQLSelect =
    'select b.summary as gitlabuser, u.id, u.username, u.realname, u.email '+
    'from '+
    '  mantis_bug_table b '+
    '  left join mantis_category_table c on b.category_id=c.id '+
    '  left join mantis_user_table u on b.reporter_id=u.id '+
    'where '+
    '  (b.project_id=:id) and (c.name=:CategoryName) '+
    'order by u.id, b.id desc '; // desc so in case of twice the same user, the last one is taken

Var
  Arr : TJSONarray;
  Src : TJSONObject;
  I,aCount : Integer;
  aUID : Int64;
  MN,MUID,GN : String;

begin
  if (FConfig.UserMapCategory='') or (FConfig.UserMapProjectID<=0) then
    begin
    DoLog('Not loading user map from mantis, no project id and category');
    Exit;
    end;
  aCount:=0;
  Arr:=FMantis.GetRecordList(SQLSelect,['ID',FConfig.UserMapProjectID,'CategoryName',FConfig.UserMapCategory],[]);
  DoLog('Checking %d user names from Mantis.',[Arr.count]);
  for I:=0 to Arr.count-1 do
    begin
    Src:=Arr.Objects[i];
    MN:=Src.Get('username','');
    GN:=Src.Get('gitlabuser','');
    if TryStrToInt64(GN,aUID) then
      begin
      MUID:=GetGitlabUserNameFromID(aUID);
      if MUID<>'' then
        begin
        GN:=MUID;
        Dolog('Converted numerical gitlab user ID %d to user name: "%s"',[aUID,GN]);
        end;
      end;
    If FuserMap.Find(MN)<>Nil then
      DoLog('Ignoring duplicate user : %s -> %s',[MN,GN])
    else
      begin
      DoLog('Adding mantis user map "%s" -> "%s"',[MN,GN]);
      FUserMap.Add(MN,GN);
      Inc(aCount);
      end;
     end;
  DoLog('Added %d user names from Mantis to user map.',[aCount]);
end;

procedure TMantisGitlabConverter.CheckConfig;

begin
  if DoPart(pExtraFieldLabels) and
    ((FConfig.ExtraFieldName='') or (FConfig.ExtraFieldLabel='')) then
    Raise EConverter.Create('Cannot convert extra fields without ExtraFieldName and ExtraFieldLabel in config');
  if DoPart(pFiles) and (FConfig.BugFileDir='') then
    Raise EConverter.Create('Cannot convert files without bugfiledir in config');
end;

function TMantisGitlabConverter.MapProjectID(aProjectID: Integer): Integer;

begin
  Result:=-1;
  if (aProjectID>0) then
    begin
    Result:=StrToIntDef(FProjectMap.Items[IntToStr(aProjectID)],-1);
    if Result=-1 then
      DoLog('Failed to map mantis project id %d to gitlab project ID',[aProjectID]);
    end;
end;

procedure TMantisGitlabConverter.WriteGitlabUserMap;

Var
  aMap : TStringList;
  I : integer;
  N : String;
  p : PtrInt;

begin
  if (FGitlabUserIDMap.Count=0) then
    exit;
  aMap:=TStringList.Create;
  try
    For I:=0 to FGitlabUserIDMap.Count-1 do
      begin
      N:=FGitlabUserIDMap[i];
      P:=PtrInt(FGitlabUserIDMap.Objects[i]);
      aMap.Add(N+'='+IntToStr(P));
      end;
    aMap.SaveToFile(FConfig.GitlabUserIDMapFile);
  finally
    aMap.Free;
  end;
end;

procedure TMantisGitlabConverter.SetNotificationLevel(const aLevel : String);

Var
  Obj : TJSONObject;

begin
  DoLog('Setting notification level to: %s',[aLevel]);
  Obj:=TJSONObject.Create(['level',aLevel]);
  try
    FGitlab.UpdateResource(Format('projects/%d/notification_settings',[Config.GitlabProjectID]),Obj);
  finally
    Obj.Free;
  end;
end;

procedure TMantisGitlabConverter.SaveNotificationLevel;

Var
  Obj : TJSONObject;

begin
  DoLog('Getting notification level');
  Obj:=FGitlab.GetSingleResource(Format('projects/%d/notification_settings',[Config.GitlabProjectID]),[]);
  try
    FNotificationlevel:=Obj.Get('level','');
    SetNotificationLevel('disabled');
  finally
    Obj.Free;
  end;
  DoLog('Got notification level: %s',[FNotificationlevel]);
end;

procedure TMantisGitlabConverter.RestoreNotificationLevel;

begin
  SetNotificationLevel(FNotificationLevel);
end;


procedure TMantisGitlabConverter.Execute;

begin
  CheckConfig;
  SaveNotificationLevel;
  Try
    if DoPart(pStandardLabels) then
      ConvertStandardLabels;
    if DoPart(PVersionLabels) or DoPart(pVersionMileStones) then
      CollectVersionStrings(FVersions, FProdVersions);
    If DoPart(pVersionLabels) then
      ConvertVersionLabels;
    if DoPart(pExtraFieldLabels) then
      ConvertExtraFieldLabel;
    If DoPart(pCategoryLabels) then
      ConvertCategoryLabels;
    If DoPart(pVersionMilestones) then
      ConvertVersionMilestones;
    If DoPart(pBugs) then
      begin
      LoadUserMap;
      LoadMantisUserMap;
      LoadProjectMap;
      LoadRevisionMap;
      ConvertBugs;
      end;
    if DoPart(pLinkIssues) then
      ConvertBugLinks;
  finally
    if (FConfig.GitlabUserIDMapFile<>'')  then
      WriteGitlabUserMap;
    RestoreNotificationLevel;
  end;
end;

end.

